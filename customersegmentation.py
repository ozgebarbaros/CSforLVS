import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
import pandas as pd

k=8

hotel_data = pd.read_csv('../data/processed_data.csv', sep=";")
hotel_data.columns = ['user_country', 'hotel_reviews', 'period_of_stay', 'traveler_type', 'score', 'pool', 'gym',
                      'tennis_court', 'spa', 'casino', 'free_internet', 'hotel_name']

matrix = hotel_data.pivot_table(index=['user_country', 'traveler_type', 'period_of_stay'],
                                columns=['hotel_name'],
                                values='score')
matrix = matrix.fillna(0).reset_index()
matrix = matrix.reset_index()

x_cols = matrix.columns[3:]
pca = PCA(n_components=3, whiten=True).fit(matrix)
matrix_pca = pca.transform(matrix)

cluster = KMeans(n_clusters=k)

matrix['cluster'] = cluster.fit_predict(matrix_pca)
C = cluster.cluster_centers_
fig = plt.figure()
ax = Axes3D(fig)
ax.scatter(matrix_pca[:, 0], matrix_pca[:, 1], matrix_pca[:, 2], c="y")
ax.scatter(C[:, 0], C[:, 1], C[:, 2], marker='*', c='#050505', s=1000)
plt.show()